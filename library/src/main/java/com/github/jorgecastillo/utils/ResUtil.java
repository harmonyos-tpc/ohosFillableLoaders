/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.jorgecastillo.utils;

import ohos.agp.components.AttrSet;
import ohos.agp.text.Font;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.Optional;

/**
 * ResUtil
 */
public class ResUtil {
    private static final String TAG = "ResUtil";

    private static final int BUFFER_LENGTH = 8192;

    private static final int DEFAULT_ERROR = -1;

    private static final String RAW_FILE_PATH = "entry/resources/rawfile/";

    private ResUtil() {
    }

    /**
     * get the dimen value
     *
     * @param context the context
     * @param id the id
     * @return get the float dimen value
     */
    public static float getDimen(WeakReference<Context> context, int id) {
        float result = 0;
        if (context == null) {
            LogUtil.error(TAG, "getDimen -> get null context");
            return result;
        }
        ResourceManager manager = context.get().getResourceManager();
        if (manager == null) {
            LogUtil.error(TAG, "getDimen -> get null ResourceManager");
            return result;
        }
        try {
            result = manager.getElement(id).getFloat();
        } catch (IOException e) {
            LogUtil.error(TAG, "getDimen -> IOException");
        } catch (NotExistException e) {
            LogUtil.error(TAG, "getDimen -> NotExistException");
        } catch (WrongTypeException e) {
            LogUtil.error(TAG, "getDimen -> WrongTypeException");
        }
        return result;
    }

    /**
     * get the String value
     *
     * @param context the context
     * @param id the id
     * @return get the String value
     */
    public static String getString( WeakReference<Context> context, int id) {
        String result = "";
        if (context == null) {
            LogUtil.error(TAG, "getString -> get null context");
            return result;
        }
        ResourceManager manager = context.get().getResourceManager();
        if (manager == null) {
            LogUtil.error(TAG, "getString -> get null ResourceManager");
            return result;
        }
        try {
            result = manager.getElement(id).getString();
        } catch (IOException e) {
            LogUtil.error(TAG, "getString -> IOException");
        } catch (NotExistException e) {
            LogUtil.error(TAG, "getString -> NotExistException");
        } catch (WrongTypeException e) {
            LogUtil.error(TAG, "getString -> WrongTypeException");
        }
        return result;
    }

    /**
     * get the Color value
     *
     * @param context the context
     * @param id the id
     * @return get the String value
     */
    public static int getColor( WeakReference<Context> context, int id) {
        int result = 0;
        if (context == null) {
            LogUtil.error(TAG, "getColor -> get null context");
            return result;
        }
        ResourceManager manager = context.get().getResourceManager();
        if (manager == null) {
            LogUtil.error(TAG, "getColor -> get null ResourceManager");
            return result;
        }
        try {
            result = manager.getElement(id).getColor();
        } catch (IOException e) {
            LogUtil.error(TAG, "getColor -> IOException");
        } catch (NotExistException e) {
            LogUtil.error(TAG, "getColor -> NotExistException");
        } catch (WrongTypeException e) {
            LogUtil.error(TAG, "getColor -> WrongTypeException");
        }
        return result;
    }

    /**
     * get the integer value
     *
     * @param context the context
     * @param id the id
     * @return get the String value
     */
    public static int getInteger( WeakReference<Context> context, int id) {
        int result = 0;
        if (context == null) {
            LogUtil.error(TAG, "getColor -> get null context");
            return result;
        }
        ResourceManager manager = context.get().getResourceManager();
        if (manager == null) {
            LogUtil.error(TAG, "getColor -> get null ResourceManager");
            return result;
        }
        try {
            result = manager.getElement(id).getInteger();
        } catch (IOException e) {
            LogUtil.error(TAG, "getColor -> IOException");
        } catch (NotExistException e) {
            LogUtil.error(TAG, "getColor -> NotExistException");
        } catch (WrongTypeException e) {
            LogUtil.error(TAG, "getColor -> WrongTypeException");
        }
        return result;
    }

    /**
     * get the string array
     *
     * @param context the context
     * @param id the string array id
     * @return the string array
     */
    public static String[] getStringArray(Context context, int id) {
        String[] results = null;
        if (context == null) {
            LogUtil.error(TAG, "getStringArray -> get null context");
            return results;
        }
        ResourceManager manager = context.getResourceManager();
        if (manager == null) {
            LogUtil.error(TAG, "getStringArray -> get null ResourceManager");
            return results;
        }
        try {
            results = manager.getElement(id).getStringArray();
        } catch (IOException e) {
            LogUtil.error(TAG, "getStringArray -> IOException");
        } catch (NotExistException e) {
            LogUtil.error(TAG, "getStringArray -> NotExistException");
        } catch (WrongTypeException e) {
            LogUtil.error(TAG, "getStringArray -> WrongTypeException");
        }
        return results;
    }

    /**
     * Obtains the Font object generated using ttf file
     *
     * @param context Context
     * @param fontFamily ttf file name
     * @return Font object
     */
    public static Font createFont(Context context, String fontFamily) {
        String path = RAW_FILE_PATH + fontFamily;
        LogUtil.error(TAG, "Font Path : " + path);
        File file = new File(context.getDataDir(), fontFamily);
        try (OutputStream outputStream = new FileOutputStream(file);
            InputStream inputStream = context.getResourceManager().getRawFileEntry(path).openRawFile()) {
            byte[] buffers = new byte[BUFFER_LENGTH];
            int bytesRead = inputStream.read(buffers, 0, BUFFER_LENGTH);
            while (bytesRead != DEFAULT_ERROR) {
                outputStream.write(buffers, 0, bytesRead);
                bytesRead = inputStream.read(buffers, 0, BUFFER_LENGTH);
            }
        } catch (FileNotFoundException exception) {
            LogUtil.error(TAG, "loadFontFromFile : FileNotFoundException");
        } catch (IOException exception) {
            LogUtil.error(TAG, "loadFontFromFile : IOException");
        }
        return Optional.of(new Font.Builder(file).setWeight(Font.REGULAR).build()).get();
    }

    /**
     * Obtains the int from AttrSet read from xml file
     *
     * @param attrSet AttrSet read from xml
     * @param key xml attribute name
     * @param defaultValue Default value
     * @return int
     */
    public static int getIntFromAttrSet(WeakReference<AttrSet> attrSet, String key, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        return attrSet.get().getAttr(key).isPresent() ? attrSet.get().getAttr(key).get().getIntegerValue() : defaultValue;
    }

    /**
     * Obtains the dimension from AttrSet read from xml file
     *
     * @param attrSet AttrSet read from xml
     * @param key xml attribute name
     * @param defaultValue Default value
     * @return float
     */
    public static float getDimenFromAttrSet(WeakReference<AttrSet> attrSet, String key, float defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        return attrSet.get().getAttr(key).isPresent() ? attrSet.get().getAttr(key).get().getDimensionValue() : defaultValue;
    }

    /**
     * Obtains the boolean from AttrSet read from xml file
     *
     * @param attrSet AttrSet read from xml
     * @param key xml attribute name
     * @param defaultValue Default value
     * @return boolean
     */
    public static boolean getBoolFromAttrSet(AttrSet attrSet, String key, boolean defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        return attrSet.getAttr(key).isPresent() ? attrSet.getAttr(key).get().getBoolValue() : defaultValue;
    }

    /**
     * Obtains the string from AttrSet read from xml file
     *
     * @param attrSet AttrSet read from xml
     * @param key xml attribute name
     * @param defaultValue Default value
     * @return String
     */
    public static String getStringFromAttrSet(AttrSet attrSet, String key, String defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        return attrSet.getAttr(key).isPresent() ? attrSet.getAttr(key).get().getStringValue().trim() : defaultValue;
    }

    /**
     * Obtains the color from AttrSet read from xml file
     *
     * @param attrSet AttrSet read from xml
     * @param key xml attribute name
     * @param defaultValue Default value
     * @return color
     */
    public static int getColorFromAttrSet( WeakReference<AttrSet> attrSet, String key, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        return attrSet.get().getAttr(key).isPresent() ? attrSet.get().getAttr(key).get().getColorValue().getValue() : defaultValue;
    }
}

