/*
 * Copyright (C) 2015 Jorge Castillo Pérez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.jorgecastillo;

import com.github.jorgecastillo.clippingtransforms.ClippingTransform;
import com.github.jorgecastillo.clippingtransforms.PlainClippingTransform;
import com.github.jorgecastillo.library.ResourceTable;
import com.github.jorgecastillo.utils.LogUtil;
import com.github.jorgecastillo.utils.ResUtil;

import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import java.lang.ref.WeakReference;

/**
 * Builder used to simplify code view construction. Normally i use to declare builders into
 * the classes that they are building, but this time i will make an exception to improve
 * readability.({@link FillableLoader} was getting a little bit big).
 */
public class FillableLoaderBuilder {

  private ComponentContainer parent;
  private ComponentContainer.LayoutConfig params;

  private int strokeColor = -1;
  private int fillColor = -1;
  private int strokeWidth = -1;
  private int originalWidth = -1;
  private int originalHeight = -1;
  private int strokeDrawingDuration = -1;
  private int fillDuration = -1;
  private boolean percentageEnabled;
  private float percentage;
  private ClippingTransform clippingTransform;
  private String svgPath;

  public FillableLoaderBuilder parentView(ComponentContainer parent) {
    this.parent = parent;
    return this;
  }

  public FillableLoaderBuilder layoutParams(ComponentContainer.LayoutConfig params) {
    this.params = params;
    return this;
  }

  public FillableLoaderBuilder strokeColor(int strokeColor) {
    this.strokeColor = strokeColor;
    return this;
  }

  public FillableLoaderBuilder fillColor(int fillColor) {
    this.fillColor = fillColor;
    return this;
  }

  public FillableLoaderBuilder strokeWidth(int strokeWidth) {
    this.strokeWidth = strokeWidth;
    return this;
  }

  public FillableLoaderBuilder originalDimensions(int originalWidth, int originalHeight) {
    this.originalWidth = originalWidth;
    this.originalHeight = originalHeight;
    return this;
  }

  public FillableLoaderBuilder strokeDrawingDuration(int strokeDrawingDuration) {
    this.strokeDrawingDuration = strokeDrawingDuration;
    return this;
  }

  public FillableLoaderBuilder fillDuration(int fillDuration) {
    this.fillDuration = fillDuration;
    return this;
  }

  public FillableLoaderBuilder clippingTransform(ClippingTransform transform) {
    this.clippingTransform = transform;
    return this;
  }

  public FillableLoaderBuilder svgPath(String svgPath) {
    this.svgPath = svgPath;
    return this;
  }

  public FillableLoaderBuilder percentage(float percentage) {
    if (percentage < 0 || percentage > 100) {
      LogUtil.error("FillableLoaderBuilder", "percentage needs to be a value from 0 to 100");
      return this;
    }
    percentageEnabled = true;
    this.percentage = percentage;
    return this;
  }

  public FillableLoader build() {
    WeakReference<Context> context = new WeakReference<Context>(parent.getContext());
    strokeColor = strokeColor == -1 ? ResUtil.getColor(context, ResourceTable.Color_strokeColor) : strokeColor;
    fillColor = fillColor == -1 ? ResUtil.getColor(context, ResourceTable.Color_fillColor) : fillColor;
    strokeWidth = strokeWidth < 0 ? (int) ResUtil.getDimen(context, ResourceTable.Float_strokeWidth) : strokeWidth;
    strokeDrawingDuration =
        strokeDrawingDuration < 0 ? ResUtil.getInteger(context, ResourceTable.Integer_strokeDrawingDuration)
            : strokeDrawingDuration;
    fillDuration = fillDuration < 0 ? ResUtil.getInteger(context, ResourceTable.Integer_fillDuration) : fillDuration;
    clippingTransform =
        clippingTransform == null ? new PlainClippingTransform() : clippingTransform;

    if (params == null) {
      throwArgumentException("layout params");
    }

    if (svgPath == null) {
      throwArgumentException("an svg path");
    }

    return new FillableLoader(parent, params, strokeColor, fillColor, strokeWidth, originalWidth,
        originalHeight, strokeDrawingDuration, fillDuration, clippingTransform, svgPath,
        percentageEnabled, percentage);
  }

  private void throwArgumentException(String neededStuff) {
    LogUtil.error("FillableLoaderBuilder", "You must provide " + neededStuff + " in order to draw the view properly.");
  }
}
