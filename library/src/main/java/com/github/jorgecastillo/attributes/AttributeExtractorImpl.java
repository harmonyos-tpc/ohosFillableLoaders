/*
 * Copyright (C) 2015 Jorge Castillo Pérez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.jorgecastillo.attributes;

import com.github.jorgecastillo.clippingtransforms.ClippingTransform;
import com.github.jorgecastillo.clippingtransforms.TransformAbstractFactory;
import com.github.jorgecastillo.clippingtransforms.TransformFactoryImpl;
import com.github.jorgecastillo.library.ResourceTable;
import com.github.jorgecastillo.utils.LogUtil;
import com.github.jorgecastillo.utils.ResUtil;

import ohos.agp.components.AttrSet;
import ohos.app.Context;

import java.lang.ref.WeakReference;

/**
 * Attribute parser abstraction to avoid ugly nomenclature into the main view class.
 *
 * @author jorge
 * @since 11/08/15
 */
public class AttributeExtractorImpl implements AttributeExtractor {

  private WeakReference<Context> weakContext;
  private WeakReference<AttrSet> weakAttrs;
  private TransformAbstractFactory transformFactory;

  private AttributeExtractorImpl(WeakReference<Context> weakContext,
      WeakReference<AttrSet> weakAttrs) {

    this.weakContext = weakContext;
    this.weakAttrs = weakAttrs;
    transformFactory = new TransformFactoryImpl();
  }

  private Context context() {
    return weakContext.get();
  }

  @Override public int getStrokeColor() {
    return ResUtil.getColorFromAttrSet(weakAttrs, ResUtil.getString(weakContext, ResourceTable.String_fl_strokeColor),
        ResUtil.getColor(weakContext, ResourceTable.Color_strokeColor));
  }

  @Override public int getFillColor() {
    return ResUtil.getColorFromAttrSet(weakAttrs, ResUtil.getString(weakContext, ResourceTable.String_fl_fillColor),
        ResUtil.getColor(weakContext, ResourceTable.Color_fillColor));
  }

  @Override public int getStrokeWidth() {
    return (int) ResUtil.getDimenFromAttrSet(weakAttrs, ResUtil.getString(weakContext, ResourceTable.String_fl_strokeWidth),
        ResUtil.getDimen(weakContext, ResourceTable.Float_strokeWidth));
  }

  @Override public int getOriginalWidth() {
    return ResUtil.getIntFromAttrSet(weakAttrs, ResUtil.getString(weakContext,  ResourceTable.String_fl_originalWidth),
        -1);
  }

  @Override public int getOriginalHeight() {
    return ResUtil.getIntFromAttrSet(weakAttrs, ResUtil.getString(weakContext,  ResourceTable.String_fl_originalHeight),
        -1);
  }

  @Override public int getStrokeDrawingDuration() {
    return ResUtil.getIntFromAttrSet(weakAttrs, ResUtil.getString(weakContext,  ResourceTable.String_fl_strokeDrawingDuration),
        ResUtil.getInteger(weakContext, ResourceTable.Integer_strokeDrawingDuration));
  }

  @Override public int getFillDuration() {
    return ResUtil.getIntFromAttrSet(weakAttrs, ResUtil.getString(weakContext,  ResourceTable.String_fl_fillDuration),
        ResUtil.getInteger(weakContext, ResourceTable.Integer_fillDuration));
  }

  public int getFillPercentage() {
    return ResUtil.getIntFromAttrSet(weakAttrs, ResUtil.getString(weakContext,  ResourceTable.String_fl_fillPercentage),
        ResUtil.getInteger(weakContext, ResourceTable.Integer_fillPercentage));
  }
  @Override public ClippingTransform getClippingTransform() {
    int value = ResUtil.getIntFromAttrSet(weakAttrs, ResUtil.getString(weakContext,
        ResourceTable.String_fl_clippingTransform), 0);
    return transformFactory.getClippingTransformFor(value);
  }

  @Override public void recycleAttributes() {
  }

  @Override public void release() {
    weakContext = null;
    weakAttrs = null;
  }

  public static class Builder {

    private WeakReference<Context> weakContext;
    private WeakReference<AttrSet> weakAttrs;

    public Builder with(Context context) {
      if (context == null) {
        LogUtil.error("AttributeExtractorImpl", "Context must not be null!");
        return null;
      }
      weakContext = new WeakReference<Context>(context);
      return this;
    }

    public Builder with(AttrSet attributeSet) {
      if (attributeSet == null) {
        LogUtil.error("AttributeExtractorImpl", "Attribute set must not be null!");
        return null;
      }
      weakAttrs = new WeakReference<AttrSet>(attributeSet);
      return this;
    }

    public AttributeExtractorImpl build() {
      return new AttributeExtractorImpl(weakContext, weakAttrs);
    }
  }
}