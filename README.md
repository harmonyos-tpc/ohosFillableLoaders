# ohosFillableLoaders

ohosFillableLoaders: openharmony fillable progress component working with SVG paths. This is a nice option too if you want to create an interesting branding logo for your app.

## ohosFillableLoaders includes :
* Customize filling using clippingTransform
* Listen to State change
* strokeColor and fillColor
* Fill Percentage.
* Fill Duration.
* Stroke Drawing Duration

# Usage Instructions

ohosFillableLoaders can be used programmatically by using FillableLoaderBuilder class. It will get automatically attached to the given parent component. Use the LayoutConfig argument to position it:

```
FillableLoaderBuilder loaderBuilder = new FillableLoaderBuilder();
fillableLoader = loaderBuilder
    .parentView((FrameLayout) rootComponent)
    .layoutParams(params)
    .svgPath(Paths.JOB_AND_TALENT)
    .originalDimensions(970, 970)
    .strokeWidth(strokeWidth)
    .strokeColor(Color.parseColor("#1c9ade"))
    .fillColor(Color.parseColor("#1c9ade"))
    .strokeDrawingDuration(2000)
    .fillDuration(5000)
    .clippingTransform(new PlainClippingTransform())
    .build();

```
This can also be done via xml as shown below :

```
    <com.github.jorgecastillo.FillableLoader
        ohos:id="$+id:fillableLoader"
        ohos:height="300vp"
        ohos:width="150vp"
        ohos:layout_alignment="center"
        app:fl_clippingTransform="$integer:plain"
        app:fl_fillColor="#4CAF50"
        app:fl_originalHeight="540"
        app:fl_originalWidth="1079"
        app:fl_strokeColor="#ffffff"
        app:fl_strokeWidth="1dp"/>
```

The only required arguments which does not have default values are the original dimensions from the svg image, and the svg path. Both of them are needed in order to get everything working properly. You can omit the other ones if you want.

### Listen to State change
In order to allow reaction to every State switch (NOT_STARTED -> TRACE_STARTED -> FILL_STARTED -> FINISHED) you must implement OnStateChangeListener and override its onStateChange(int state) method.
```
@Override public void onStateChange(int state) {
  showStateHint(state);

  switch(state) {
    case State.FILL_STARTED:
      ...
      break;
    case State.FINISHED:
      ...
  }
}
```

### Customize filling
To get a custom "top border" style for the filling figure, you can implement the ClippingTransform interface, which will force you to create an implementation for the transform() method.

You must think about the clipping figure as an invisible polygon that is going to clip your filling figure. (Like a DIFFERENCE operation between the total filling space and the custom transform figure you are applying). It must vary depending on the currentFillPhase.
```
public class PlainClippingTransform implements ClippingTransform {

  @Override public void transform(Canvas canvas, float currentFillPhase, Component component) {
    canvas.clipRect(0, (component.getBottom() - component.getTop()) * (1f - currentFillPhase),
        component.getRight(), component.getBottom());
  }
}
```
The canvas would be the one used to draw the loader, the currentFillPhase argument is the current percent of the animation step (from 0 to 1), and the component would need to be provided too, so it can be used to create an animation based on component properties, like its current dimensions.

### Custom behavior
If your loader / brand logo needs to you can suppress the stroke drawing animation and go directly for the filling one. To do that, just set app:fl_strokeDrawingDuration="0".

If you only need to fill the pattern partially or you want to control the fill progress, you can use fl_fillPercentage xml (resource) property or if you want to control from Java use.
```
fillableLoader.setPercentage(percent);
```

# Installation Instructions

1.For using ohosFillableLoaders module in sample app, include the source code and add the below dependencies in entry/build.gradle to generate hap/library.har.

```
dependencies {
    implementation project(':library')
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testCompile 'junit:junit:4.12'
    }
```

2.For using ohosFillableLoaders in separate application using har file, add the har file in the entry/libs folder and add the dependencies in entry/build.gradle file.

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testCompile 'junit:junit:4.12'
}
```

3. For using ohosFillableLoaders from the remote repository, add the below dependency in "entry" build.gradle.

```
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:ohosFillableLoaders:1.0.0'
}
```   

# License
```
Copyright 2015 Jorge Castillo Pérez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```