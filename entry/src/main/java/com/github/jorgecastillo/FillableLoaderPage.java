/*
 * Copyright (C) 2015 Jorge Castillo Pérez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.jorgecastillo;

import com.github.jorgecastillo.clippingtransforms.WavesClippingTransform;
import com.github.jorgecastillo.listener.OnStateChangeListener;
import com.github.jorgecastillo.utils.ResUtil;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;

import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.lang.ref.WeakReference;

/**
 * @author jorge
 * @since 11/08/15
 */
public class FillableLoaderPage extends AbilitySlice implements OnStateChangeListener {
    FillableLoader fillableLoader;

    private Text textTitle;

    private Text textState;

    private Component rootView;

    private int mPercentage = 20;

    private int pageNumber;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        pageNumber = intent.getIntParam("page_number", 0);
        setUIContent((ComponentContainer) loadView());
        setupFillableLoader(pageNumber);
        textTitle.setText(getPageTitle());
        fillableLoader.reset();
    }

    @Override
    protected void onActive() {
        super.onActive();
        new EventHandler(EventRunner.getMainEventRunner()).postTask(() -> {
            fillableLoader.start();
        }, 250);
    }

    private Component loadView() {
        switch (pageNumber) {
            case 0:
                rootView = LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_fillable_loader_first_page, null, false);
                break;
            case 1:
                rootView = LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_fillable_loader_second_page, null, false);
                break;
            case 2:
                rootView = LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_fillable_loader_third_page, null, false);
                break;
            case 3:
                rootView = LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_fillable_loader_fourth_page, null, false);
                break;
            case 4:
                rootView = LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_fillable_loader_fifth_page, null, false);
                break;
            case 5:
                rootView = LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_fillable_loader_sixth_page, null, false);
                break;
            default:
                rootView = LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_fillable_loader_fifth_page, null, false);
                break;
        }
        fillableLoader = (FillableLoader) rootView.findComponentById(ResourceTable.Id_fillableLoader);
        textTitle = (Text) rootView.findComponentById(ResourceTable.Id_title);
        textState = (Text) rootView.findComponentById(ResourceTable.Id_state);
        return rootView;
    }

    private void setupFillableLoader(int pageNum) {
        if (pageNum == 2) {
            initTitle();
            FillableLoaderBuilder loaderBuilder = new FillableLoaderBuilder();
            fillableLoader = loaderBuilder.parentView((DirectionalLayout) rootView)
                .svgPath(Paths.JOB_AND_TALENT)
                .layoutParams(getFlLayoutConfig())
                .originalDimensions(970, 970)
                .strokeColor(AttrHelper.convertValueToColor("#1c9ade").getValue())
                .fillColor(AttrHelper.convertValueToColor("#1c9ade").getValue())
                .strokeDrawingDuration(2000)
                .clippingTransform(new WavesClippingTransform())
                .fillDuration(10000)
                .build();
            initState();
        } else if (pageNum == 4) {
            Slider slider = (Slider) rootView.findComponentById(ResourceTable.Id_slider);
            slider.setProgressValue(mPercentage);
            slider.setValueChangedListener(new Slider.ValueChangedListener() {
                @Override
                public void onProgressUpdated(Slider slider, int progress, boolean bool) {
                    mPercentage = progress;
                    fillableLoader.setPercentage(progress);
                }

                @Override
                public void onTouchStart(Slider slider) {
                }

                @Override
                public void onTouchEnd(Slider slider) {
                }
            });
            initTitle();
            FillableLoaderBuilder loaderBuilder = new FillableLoaderBuilder();
            fillableLoader = loaderBuilder.parentView((DirectionalLayout) rootView)
                .svgPath(Paths.JOB_AND_TALENT)
                .layoutParams(getFlLayoutConfig())
                .percentage(mPercentage)
                .originalDimensions(970, 970)
                .strokeColor(AttrHelper.convertValueToColor("#1c9ade").getValue())
                .fillColor(AttrHelper.convertValueToColor("#1c9ade").getValue())
                .strokeDrawingDuration(2000)
                .clippingTransform(new WavesClippingTransform())
                .fillDuration(10000)
                .build();
            initState();
        } else {
            fillableLoader.setSvgPath(pageNum == 0
                ? Paths.INDOMINUS_REX
                : pageNum == 1
                    ? Paths.RONALDO
                    : Paths.GITHUB);
        }
        fillableLoader.setOnStateChangeListener(this);
    }

    @Override
    public void onStateChange(int state) {
        showStateHint(state);
    }

    private DirectionalLayout.LayoutConfig getFlLayoutConfig() {
        WeakReference<Context> weakContext = new WeakReference<Context>(this);
        int componentSize = (int) ResUtil.getDimen(weakContext,
            com.github.jorgecastillo.ResourceTable.Float_fourthSampleViewSize);
        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(componentSize, componentSize);
        params.alignment = LayoutAlignment.CENTER;
        return params;
    }

    private void initTitle() {
        textTitle = new Text(this);
        textTitle.setTextColor(Color.WHITE);
        textTitle.setTextAlignment(TextAlignment.CENTER);
        textTitle.setTextSize(20, Text.TextSizeType.FP);
        ((ComponentContainer) rootView).addComponent(textTitle, getTextLayoutConfig());
    }

    private void initState() {
        textState = new Text(this);
        textState.setTextColor(Color.WHITE);
        textState.setTextAlignment(TextAlignment.CENTER);
        textState.setTextSize(20, Text.TextSizeType.FP);
        ((ComponentContainer) rootView).addComponent(textState, getTextLayoutConfig());
    }

    private DirectionalLayout.LayoutConfig getTextLayoutConfig() {
        DirectionalLayout.LayoutConfig textConfig = new DirectionalLayout.LayoutConfig(
            ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        textConfig.alignment = LayoutAlignment.CENTER;
        textConfig.setMarginsTopAndBottom(100, 100);
        return textConfig;
    }

    private void showStateHint(int state) {
        textState.setText("State changed callback: " + state);
    }

    private String getPageTitle() {
        switch (pageNumber) {
            case 0:
                return "Plain";
            case 1:
                return "Stroke";
            case 2:
                return "Rounded";
            case 3:
                return "Waves";
            case 4:
                return "Waves Percentage";
            case 5:
                return "Waves 40%";
            default:
                return "Waves Percentage";
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        fillableLoader.setOnStateChangeListener(null);
    }
}
