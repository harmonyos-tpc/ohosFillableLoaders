/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.jorgecastillo.slice;

import com.github.jorgecastillo.FillableLoaderPage;
import com.github.jorgecastillo.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * MainAbilitySlice extends AbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initButtons();
    }

    private void initButtons() {
        Button test1 = (Button) findComponentById(ResourceTable.Id_test_1);
        test1.setClickedListener(this);
        Button test2 = (Button) findComponentById(ResourceTable.Id_test_2);
        test2.setClickedListener(this);
        Button test3 = (Button) findComponentById(ResourceTable.Id_test_3);
        test3.setClickedListener(this);
        Button test4 = (Button) findComponentById(ResourceTable.Id_test_4);
        test4.setClickedListener(this);
        Button test5 = (Button) findComponentById(ResourceTable.Id_test_5);
        test5.setClickedListener(this);
    }

    private void gotoPage(int pageNum) {
        present(new FillableLoaderPage(), new Intent().setParam("page_number", pageNum));
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_test_1:
                gotoPage(0);
                break;
            case ResourceTable.Id_test_2:
                gotoPage(1);
                break;
            case ResourceTable.Id_test_3:
                gotoPage(2);
                break;
            case ResourceTable.Id_test_4:
                gotoPage(3);
                break;
            case ResourceTable.Id_test_5:
                gotoPage(4);
                break;
        }
    }
}
